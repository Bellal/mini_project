<?php 
session_start();
include_once("vendor/autoload.php");
use \App\Mini_Project\Message\Message;
use \App\Mini_Project\Utility\Utility;
?>
                          
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css" media="all" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-camera"></span>&nbsp;<span>Mini Project</span></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a></li>
                </ul>
   
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div>
                <?php
                if((array_key_exists('Message',$_SESSION))&&!empty($_SESSION['Message'])){
                    echo Message::flush();
                    }
                    ?>
            </div> 
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                                <div class="panel-heading">Registration</div>
                                <div class="panel-body">
                                    <div class="col-md-11">
                                    <form role="form" action="views/user/store.php" method="post" autocomplete="on">
                                            <div class="row">
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                        <input type="text" name="firstname" class="form-control" id="fname" placeholder="First Name" required >
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-6">
                                                    <input type="text" name="lastname" class="form-control" id="lname" placeholder="Last Name" >
                                                </div>
                                            </div>
                                        <div class="row">
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-envelope"></span></span>
                                                        <input type="email" name="email" class="form-control" placeholder="example@example.com"  >
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-phone-alt"></span></span>
                                                        <input type="text" name="mobile" class="form-control" placeholder="+88017XXXXXXXX"  >
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="row">
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input type="date" name="dob" class="form-control" id="dob" >
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        <div class="form-group"">
                                                    Gender : <label class="radio-inline"><input type="radio" name="gender"value="Male">Male</label>
                                                    <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
                                                </div><br>
                                                 <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                        <input type="text" name="username" class="form-control" placeholder="Enter Your Username"  >
                                                    </div>
                                                <br>
                                                 <div class="row">
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-lock"></span></span>
                                                        <input type="password" name="password" class="form-control" placeholder="XXXXXXXX"  >
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-lock"></span></span>
                                                        <input type="password" name="password" class="form-control" placeholder="XXXXXXXX"  >
                                                    </div>
                                                </div>
                                            </div>
                                                <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                  </form>		
                                </div>

                                </div>
                              </div> 
                </div>
                <div class="col-sm-6">
                   <div class="panel panel-primary">
                                <div class="panel-heading">Login Panel</div>
                                <div class="panel-body">
                                    <div class="col-md-10">
                                    <form role="form" action="views/login/login.php" method="post" autocomplete="off">
                                        <div class="form-group">
                                  <div class="input-group">
                                      <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-user"></span></span>
                                      <input type="text" name="username" class="form-control" id="fname" placeholder="username or email" required >
                                  </div>
                                            </div>
                                        <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="addon1"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input type="password" name="password" class="form-control" placeholder="XXXXXXXX">
                                    </div>
                                        </div>
                                    <button type="submit" class="btn btn-success">Login</button>
                                  </form>		
                                </div>

                                </div>
                              </div> 
                </div>
            </div>
        </div>
    <script src="../../resource/js/jquery.dataTables.min.js"></script>
        
        <script>
            $('.alert').fadeOut(4000);
        </script>

    </body>
</html>