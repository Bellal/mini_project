<?php 
session_start();
include_once("../../vendor/autoload.php");
use \App\Mini_Project\Message\Message;
use \App\Mini_Project\Utility\Utility;
use \App\Mini_Project\login\login;
$check=Login::is_loggedin();
if(!$check){
    Message::set('<div class="alert alert-warning"><strong>To view the content, you must log in!</strong> </div>');
    Utility::redirect("../../index.php");
}
?>
                          
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap-theme.min.css" media="all" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-camera"></span>&nbsp;<span>Mini Project</span></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a></li>
                    <li><a href="../phonebook/index.php"><span class="glyphicon glyphicon-home"></span>Phonebook</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['username'];?><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.php">View Profile</a></li>
                            <li><a href="edit.php">Update Profile</a></li>
                            <li><a href="../login/logout.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp;LogOut</a></li>
                        </ul>
                    </li>
                </ul>
   
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div>
                <?php
                if((array_key_exists('Message',$_SESSION))&&!empty($_SESSION['Message'])){
                    echo Message::flush();
                    }
                    ?>
            </div> 
            Welcome
        </div>
    <script src="../../resource/js/jquery.dataTables.min.js"></script>
        
        <script>
            $('.alert').fadeOut(4000);
        </script>

    </body>
</html>