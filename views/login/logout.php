<?php
session_start();
include_once("../../vendor/autoload.php");
use \App\Mini_Project\Message\Message;
use \App\Mini_Project\Utility\Utility;
session_destroy();
$_SESSION=array();
Message::set('<div class="alert alert-success"><strong>You are successfully logged out !</strong> </div>');
Utility::redirect("../../index.php");

?>
