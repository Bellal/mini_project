<?php
session_start();
include_once("../../" . "vendor/autoload.php");

use \App\Mini_Project\Message\Message;
use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;
$contact_of=$_SESSION['username'];
$obj = new Phone_book();
$var = $obj->index($contact_of);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book Information List</title>
        <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h2{
            color : rosybrown;
        }
    </style>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-camera"></span>&nbsp;<span>Mini Project</span></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://localhost/login/views/dashboard/index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a></li>
                    <li><a href="../phonebook/index.php"><span class="glyphicon glyphicon-home"></span>Phonebook</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['username'];?><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.php">View Profile</a></li>
                            <li><a href="edit.php">Update Profile</a></li>
                            <li><a href="../login/logout.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp;LogOut</a></li>
                        </ul>
                    </li>
                </ul>
   
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h2>Phone Book List</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Address</th>
                                <th>Action</th>
                                <th>Action</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($var as $pbook) {
                                ?>
                                <tr>
                                    <td><?php echo $pbook['name'] ?></td>
                                    <td><?php echo $pbook['email'] ?></td>
                                    <td><?php echo $pbook['mobile'] ?></td>
                                    <td><?php echo $pbook['address'] ?></td>
                                    <td><button type="submit" class="btn btn-warning"><a href="edit.php?id=<?php echo $pbook['id']; ?>">Update</a></button> </td>
                                    <td><button type="submit" class="btn btn-info"><a href="show.php?id=<?php echo $pbook['id'];?>">View</a></button> </td>
                                    <td>    
                                        <form action="delete.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $pbook['id']; ?>">
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="create.php"><b>Entry Phone Book Data</b></a>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>