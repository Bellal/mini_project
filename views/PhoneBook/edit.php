<?php
session_start();
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\Message\Message;
use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;
use \App\Mini_Project\login\login;
$check=Login::is_loggedin();
if(!$check){
    Message::set('<div class="alert alert-warning"><strong>To view the content, you must log in!</strong> </div>');
    Utility::redirect("../../index.php");
}

$obj = new Phone_book();
$thepb = $obj->edit($_GET['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Update Information</title>
        <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h3{
            color : rosybrown;
        }
    </style>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-camera"></span>&nbsp;<span>Mini Project</span></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://localhost/login/views/dashboard/index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a></li>
                    <li><a href="../phonebook/index.php"><span class="glyphicon glyphicon-home"></span>Phonebook</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['username'];?><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.php">View Profile</a></li>
                            <li><a href="edit.php">Update Profile</a></li>
                            <li><a href="../login/logout.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp;LogOut</a></li>
                        </ul>
                    </li>
                </ul>
   
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h3>Phone Book Information</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form role="form"  action="update.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $thepb->id;?>" />
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" value="<?php echo $thepb->name ?>" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" name="email"  value="<?php echo $thepb->email ?>" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile</label>
                            <input type="text" name="mobile"   value="<?php echo $thepb->mobile ?>" class="form-control" id="mobile">
                        </div>
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <textarea name="address" class="form-control" rows="3" id="address"><?php echo $thepb->address ?></textarea>
                        </div>

                        <div class="btn-group">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="submit" class="btn btn-warning"><a href="index.php">List</a></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>