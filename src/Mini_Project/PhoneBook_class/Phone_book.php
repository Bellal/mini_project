<?php

namespace App\Mini_Project\PhoneBook_class;


use \App\Mini_Project\Utility\Utility;
use \App\Mini_Project\Message\Message;

class Phone_book {

    public $id = "";
    public $name = "";
    public $email = "";
    public $mobile = "";
    public $address = "";
    public $contact_of = "";
    

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("Can not connect Database");
        $lnk = mysql_select_db("mini_project_1") or die("can not connect table");
    }

    public function index($contact_of) {
        $_info = array();

        $query = "SELECT * FROM phone_book WHERE contact_of='$contact_of'";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_info[] = $row;
        }

        return $_info;
    }

    public function store($data = array()) {
        $query = "INSERT INTO `phone_book` (`name`,  `email` ,`mobile`,  `address`, contact_of) VALUES ('" . $this->name . "', '" . $this->email . "', '" . $this->mobile . "', '" . $this->address . "','" . $this->contact_of . "')";

        if (mysql_query($query)) {
            Message::set('Book data is added successfully.');
        } else {
            Message::set('There is an error while storing book information, please try again.');
        }


         Utility::redirect('index.php');
    }

    public function show($id = null){
        
        if(is_null($id)){
            return;
        }
        
        $query = "SELECT * FROM `phone_book` WHERE id = ".$id;
       
        $result = mysql_query($query); 
        $phnbook = mysql_fetch_object($result);
        
        return $phnbook;
        
    }

    public function edit($id = null) {

        if (is_null($id)) {
            return;
        }

        $query = "SELECT * FROM `phone_book` WHERE id = " . $id;

        $result = mysql_query($query);
        $phoneBook = mysql_fetch_object($result);

        return $phoneBook;
    }

    public function update() {

        $query = "UPDATE `phone_book` SET `name` = '" . $this->name . "', `email` = '" . $this->email . "', `mobile` = '" . $this->mobile . "', `address` = '" . $this->address . "' WHERE `id` = " . $this->id;
        //var_dump($query);
        //die();
        if (mysql_query($query)) {
            //Message::set('Book data is updated successfully.');
        } else {
            //Message::set('There is an error while storing book information, please try again.');
        }


        Utility::redirect('index.php');
    }

        public function delete($id = null){
        
        if(is_null($id)){
            return;
        }
        
        $query = "DELETE FROM `phone_book` WHERE `id`=".$id;
       
        if( mysql_query($query)){
           // Message::set('Book data is deleted successfully.');
        }else{
            //Message::set('There is an error while deleting book information, please try again.');
        }
        
        
        Utility::redirect('index.php');
        
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data)) {
            $this->name = $data['name'];
            $this->email = $data['email'];
            $this->mobile = $data['mobile'];
            $this->address = $data['address'];
            if (array_key_exists('contact_of', $data) && !empty($data['contact_of'])) {
                $this->contact_of = $data['contact_of'];
            }

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}

?>